import express from "express";
import mongoose from "mongoose";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

const router = express.Router();
import User from "../models/user";

router.get("/", async (req, res, next) => {
  try {
    const users = await User.find().exec();
    res.status(200).json(users);
  } catch (e) {
    res.status(500).send();
  }
});

router.post("/signup", async (req, res, next) => {
  // TODO: custom validator in model for username and mail
  // https://mongoosejs.com/docs/validation.html#async-custom-validators
  bcrypt.hash(req.body.password, 10, async (err, hash) => {
    if (err) {
      return res.status(500).json({
        error: err,
      });
    } else {
      const user = new User({
        _id: new mongoose.Types.ObjectId(),
        username: req.body.username,
        email: req.body.email,
        password: hash,
      });

      try {
        await user.save();
        res.status(201).send();
      } catch (e) {
        res.status(500).json({ error: e });
      }
    }
  });
});

router.post("/signin", async (req, res, next) => {
  try {
    const user = await User.findOne({
      email: req.body.usernameOrEmail,
    }).exec();
    if (!user) {
      return res.status(401).send(); // send 401 (instead of 404) to prevent brute forcing
    } else {
      bcrypt.compare(req.body.password, user.password, (err, result) => {
        if (result) {
          const token = jwt.sign(
            { userId: user._id, email: user.email },
            process.env.JWT_PRIVATE_KEY,
            { expiresIn: "1h" }
          );
          return res.status(200).json({
            token: token,
          });
        }
        return res.status(401).send();
      });
    }
  } catch (e) {
    return res.status(401).json({ error: "Authentification failed" });
  }
});

router.delete("/:userId", async (req, res, next) => {
  try {
    await User.remove({ _id: req.params.userId }).exec();
    res.status(200).send();
  } catch (e) {
    res.status(500).json({ error: e });
  }
});

module.exports = router;
