# Flashcards API

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:3000/`. The app will automatically reload if you change any of the source files.

## Swagger

An exhaustive description of all endpoints can be viewed under `/api-docs`.
